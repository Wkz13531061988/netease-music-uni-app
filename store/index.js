import Vue from "vue"
import Vuex from "vuex"
Vue.use(Vuex)
export default new Vuex.Store({
	state:{
		topListIds:[],
		nextId:''
	},
	mutations:{
		initTopListIds(state,playLoad){
			state.topListIds=playLoad;
		},
		NEXT_ID(state , playLoad){
					for(var i=0; i<state.topListIds.length;i++ ){
						if( state.topListIds[i].id == playLoad ){
							state.nextId = state.topListIds[i+1].id;
						}
					}
				}
	},
})