import { baseUrl } from './config.js'
export function topList(){
	return new Promise(function(reslove,reject){
		uni.request({
			url:`${baseUrl}/toplist/detail`,
			success:res => {
				let result =res.data.list;
				result.length=4;
				reslove(result)
			}
		})
	})
	
}
export function list(id){
	return uni.request({
		url:`${baseUrl}/playlist/detail?id=${id}`,
		
	})
}
export function songDetail(id){
	return uni.request({
		url:`${baseUrl}/song/detail?ids=${id}`,
		
	})
}
export function songSimi(id){
	return uni.request({
		url:`${baseUrl}/simi/song?id=${id}`,
		
	})
}
export function musicComment(id){
	return uni.request({
		url:`${baseUrl}/comment/music?id=${id}`,
		
	})
}
export function lyric(id){
	return uni.request({
		url:`${baseUrl}/lyric?id=${id}`,
		
	})
}
export function songUrl(id){
	return uni.request({
		url:`${baseUrl}/song/url?id=${id}`,
		
	})
}
export function searchHot(){
	return uni.request({
		url:`${baseUrl}/search/hot/detail`,
		
	})
}
export function searchWord(word){
	return uni.request({
		url:`${baseUrl}/search?keywords=${word}`,
		
	})
}
export function searchSuggest(word){
	return uni.request({
		url:`${baseUrl}/search/suggest?keywords=${word}&type=mobile`,
		
	})
}