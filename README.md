# 网易音乐uni-app

#### 介绍
使用了网上的网易云音乐接口，基于uni-app写的小程序和h5的前端项目，实现了部分功能

#### 软件架构
先去我另一个仓库把后台接口复制下来，文本内有说明如何启动安装
https://gitee.com/Wkz13531061988/netease-music-background


#### 安装教程

先去我另一个仓库把后台接口复制下来，文本内有说明如何启动安装
https://gitee.com/Wkz13531061988/netease-music-background
本项目：
git clone （本仓库地址）
之后再用HBuilder X 打开它启动即可
微信小程序源文件在unpackage\dist\dev下，名字为mp-weixin
记得一定要先启动后台，前端项目才可以启动

####说明
实现的功能：
搜索歌曲
歌曲详情（听歌、评论等等均有）
歌单详情
首页推荐歌单


